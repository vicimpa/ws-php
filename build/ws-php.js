"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const path = require("path");
const stream = require("stream");
const child_process_1 = require("child_process");
class WsPhp {
    constructor(fileName) {
        this._pathToFile = path.join(__dirname, '../lib/bin/php');
        this._scriptFile = path.join(process.cwd(), fileName);
        this._spawnProcess = child_process_1.exec(this._pathToFile, {
            cwd: path.dirname(this._scriptFile)
        });
        this.stdin = this._spawnProcess.stdin;
        this.stdout = this._spawnProcess.stdout;
        this.stderr = this._spawnProcess.stderr;
    }
    write(row) {
        this.stdin.write(row + '\n');
    }
    primitiveValue(value) {
        if (typeof value === 'number')
            return value.toString();
        if (typeof value === 'string')
            return `"${value}"`;
        if (value instanceof Date)
            return `${(+value)}`;
        let array = [], stringValue = '';
        if (value instanceof Array)
            for (let val of value)
                array.push(this.primitiveValue(val));
        else
            for (let key in value)
                array.push(`"${key}"=>${this.primitiveValue(value[key])}`);
        stringValue += '[';
        stringValue += array.join(',');
        stringValue += ']';
        return stringValue;
    }
    sendData(datas) {
        this.write('<?php');
        for (let key in datas)
            this.write(`$${key}=${this.primitiveValue(datas[key])};`);
        this.write('?>');
        fs.createReadStream(this._scriptFile).pipe(this.stdin);
    }
    static render(fileName, datas, cb = false, charset = null) {
        let wsphp = new WsPhp(fileName);
        let getData = (resolve, reject = (err) => { throw err; }) => {
            let Data = new Buffer('', 'utf-8');
            let DataError = new Buffer('', 'utf-8');
            wsphp.stdout.on('data', (d) => Data = Buffer.concat([Data, new Buffer(d, 'utf-8')]));
            wsphp.stdout.on('close', () => resolve(charset ? Data.toString(charset) : Data));
            wsphp.stderr.on('data', (d) => DataError = Buffer.concat([DataError, new Buffer(d, 'utf-8')]));
            wsphp.stderr.on('close', () => { if (DataError.toString('utf-8').length)
                throw new Error(DataError.toString('utf-8')); });
            wsphp.sendData(datas);
        };
        if (cb instanceof stream.Writable) {
            wsphp.stdout.pipe(cb);
            wsphp.sendData(datas);
            return null;
        }
        if (cb === false)
            return new Promise((resolve, reject) => {
                getData((buff) => resolve(buff));
            });
        if (cb instanceof Function) {
            getData(cb);
            return null;
        }
    }
}
exports.WsPhp = WsPhp;
