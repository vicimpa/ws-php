ws-php
==========

PHP templater for NodeJS

# Installation
> `npm install --save ws-php`

# Create PHP script 'script.php'
```php
    <ul>
        <? foreach($data as $key => $value): ?>
            <li>
                <b>Name:</b><span><?=$value['name']?></span>
                </br>
                <b>Age:</b><span><?=$value['age']?></span>
            </li>
        <? endforeach; ?>
    </ul>
```

# Require module and send method 'render'
```js
    const php = require('ws-php').WsPhp;

    let data = {
        list: [
            {
                name: 'Name',
                age: 12
            },
            {
                name: 'Name',
                age: 12
            },
            {
                name: 'Name',
                age: 12
            }
        ]
    };

    // Stream
    php.render('./script.php', data, process.stdout);

    // Promise
    php.render('./script.php', data)
        .then((d) => console.log(d.toString()))
        .catch(error => console.log(error));

    // Callback
    php.render('./script.php', data, (d) => {
        console.log(d.toString('utf-8'));
    });
```